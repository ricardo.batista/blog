FROM openjdk:8-alpine
RUN apk add --no-cache bash
COPY target/universal/blog*.zip /tmp/blog.zip
RUN mkdir /server && unzip /tmp/blog.zip -d /server
WORKDIR /server
EXPOSE 80 443
CMD blog-0.0.0/bin/blog
