enablePlugins(JavaAppPackaging)
enablePlugins(SbtTwirl)

name            := "blog"
version         := "0.0.0"
organization    := "com.rjbatista"
scalaVersion    := "2.12.8"
maintainer      := "emacs.copo@gmail.com"

libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.7"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.20"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.20"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.1.7" % "test"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.20" % "test"
libraryDependencies += "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.20" % "test"
