package com.rjbatista

import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{ Matchers, FeatureSpec, GivenWhenThen }

import Directives._

import com.rjbatista.router.Routes

class HomePageTestKitSpec extends FeatureSpec with Matchers with GivenWhenThen
  with ScalatestRouteTest with Routes {

  info("As a blog visitor")
  info("I want read hello world on the homepage")
  feature("Hello world on the homepage") {
    scenario("The user visites the homepage and reads hello world") {
      Given("The homepage url")
      val url = "https://www.example.com"
      When("The user vistits the page")
      Get(url) ~> https ~> check {
        Then("The user reads \"Hello World!\"")
        responseAs[String] should include ("Hello World!")
      }
    }
  }
}
