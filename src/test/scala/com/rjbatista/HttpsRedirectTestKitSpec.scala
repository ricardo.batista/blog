package com.rjbatista

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{ Matchers, FeatureSpec, GivenWhenThen }

import StatusCodes.MovedPermanently
import Directives._

import com.rjbatista.router.Routes

class HttpsRedirectTestKitSpec extends FeatureSpec with Matchers with GivenWhenThen
  with ScalatestRouteTest with Routes {

  info("As a blog visitor")
  info("I want my browser to be redirect to a https schema when I choose http")

  feature("http redirect to https") {
    scenario("The user uses an http url") {
      Given("a http url")
      val url = "http://www.example.com"

      When("The user vistits the page")
      Get(url) ~> https ~> check {
        Then("The user is redirected to https")
        status shouldEqual MovedPermanently
        header[Location] shouldEqual Some(Location(Uri("https://www.example.com")))
      }
    }
  }
}
