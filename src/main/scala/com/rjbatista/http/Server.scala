package com.rjbatista.http

import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyStore
import java.security.SecureRandom
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory

import akka.actor.Actor
import akka.actor.Props
import akka.http.scaladsl.ConnectionContext
import akka.http.scaladsl.Http
import akka.http.scaladsl.HttpsConnectionContext
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory


import com.rjbatista.router.Routes
object Server {
  def props(): Props = Props(new Server())
}

class Server extends Actor with Routes {
  implicit val system = context.system
  implicit val materializer = ActorMaterializer()(context)

  val config: Config = ConfigFactory.load()
  val password: Array[Char] = config.getString("app.config.ssl.keystore.password").toCharArray
  val keystorePath = config.getString("app.config.ssl.keystore.path")
  val ks: KeyStore = KeyStore.getInstance("PKCS12")
  val keystore: InputStream = Files.newInputStream(Paths.get(keystorePath))
  ks.load(keystore, password)
  val managerInstanceName: String = "SunX509"

  val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance(managerInstanceName)
  keyManagerFactory.init(ks, password)

  val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(managerInstanceName)
  tmf.init(ks)

  val sslContext: SSLContext = SSLContext.getInstance("TLS")
  sslContext.init(keyManagerFactory.getKeyManagers,tmf.getTrustManagers, new SecureRandom)
  val ssl: HttpsConnectionContext = ConnectionContext.https(sslContext)
  Http().bindAndHandle(https,"0.0.0.0", 443, connectionContext = ssl)
  Http().bindAndHandle(https, "0.0.0.0", 80)

  def receive = {
    case any => println(any)
  }
}
