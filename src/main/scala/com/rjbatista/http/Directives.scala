package com.rjbatista.http

import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Route
import play.twirl.api.Html

object Directives {

  def ok(content: Html): Route = ok(content.toString())

  def ok(content: String): Route = {
    complete(
      HttpEntity(ContentTypes.`text/html(UTF-8)`, content)
    )
  }
}
