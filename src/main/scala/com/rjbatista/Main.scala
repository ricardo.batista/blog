package com.rjbatista

import com.rjbatista.http.Server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

object Main {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("blog")
    implicit val material: ActorMaterializer = ActorMaterializer()
    system.actorOf(Server.props());
  }
}
