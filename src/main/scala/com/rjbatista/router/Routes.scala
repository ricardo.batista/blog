package com.rjbatista.router

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import StatusCodes.MovedPermanently

import com.rjbatista.http.Directives._
import views.html._


trait Routes {
  val http: Route = get {
    pathEndOrSingleSlash {
      ok(home.index())
    }
  }

  val https: Route = scheme("http") {
      extract(_.request.uri) { uri =>
        redirect(uri.copy(scheme = "https"), MovedPermanently)
      }
  } ~ http
}
